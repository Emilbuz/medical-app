@extends("admin.layouts.master")
@section("content")
    <div class="welcome-message text-center">
        <h1 style="margin-top: 50px;">Bine ai venit {{Auth::user() -> first_name}} {{Auth::user() -> last_name}}</h1>
    </div>
@endsection