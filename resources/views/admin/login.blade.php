@extends('admin.layouts.master')

@section('title')
    Medical App - Login
@endsection

@section('content')
    <div class="login-wrapper">
        <div class="container">
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('success') }}
                </div>
            @elseif (Session::has('danger'))
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('danger') }}
                </div>
            @endif
            <div class="card card-container">
                <img id="profile-img" class="profile-img-card" src="{{ asset('img/dummy_avatar.png') }}" />
                <p id="profile-name" class="profile-name-card"></p>
                <form class="form-signin" action="{{ URL::to('login') }}" method="POST">
                    <span id="reauth-email" class="reauth-email"></span>
                    <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Adresa de email" required autofocus>
                    <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Parola" required>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Autentifica-te</button>
                </form><!-- /form -->
            </div><!-- /card-container -->
        </div>
    </div><!-- /container -->
@endsection
<?php /*
<div id="remember" class="checkbox">
    <label>
        <input type="checkbox" value="remember-me"> Remember me
    </label>
</div>
<a href="#" class="forgot-password">
    Forgot the password?
</a> */ ?>