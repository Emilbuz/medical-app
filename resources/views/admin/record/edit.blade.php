@extends('admin.layouts.master')

@section('title')
    Fisa Medicala {{ $patient->first_name . ' ' . $patient->last_name }}
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3> Fisa Medicala {{ $patient->first_name . ' ' . $patient->last_name }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-container">
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('success') }}
                    </div>
                @elseif (Session::has('danger'))
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('danger') }}
                    </div>
                @endif

                <form action="{{ URL::to('medical_record/update/' . $patient->medicalRecord->id) }}" method="POST">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 {{ $errors->has('cnp') ? ' has-error' : '' }}">
                            <label for="cnp">Cod Numeric Personal</label>
                            <input id="cnp" type="text" value="{{ $patient->cnp }}" name="cnp" class="form-control" placeholder="Cod Numeric Personal">
                            @if ($errors->has('cnp'))
                                <span class="help-block">{{ $errors->first('cnp') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12  {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Adresa de email</label>
                            <input id="email" type="email" name="email" value="{{ $patient->email }}" class="form-control" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12 {{ $errors->has('dob') ? ' has-error' : '' }}">
                            <label for="dob">Data nasterii</label>
                            <input id="dob" type="text" id="datepicker" class="form-control" name="dob" placeholder="Data nasterii" value={{ isset($patient->medicalRecord) ?  $patient->medicalRecord->dob : '' }}>
                            @if ($errors->has('dob'))
                                <span class="help-block">{{ $errors->first('dob') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12 {{ $errors->has('age') ? ' has-error' : '' }}">
                            <label for="age">Varsta</label>
                            <input id="age" type="text" name="age" class="form-control" placeholder="Varsta" value={{ isset($patient->medicalRecord) ? $patient->medicalRecord->age : '' }}>
                            @if ($errors->has('age'))
                                <span class="help-block">{{ $errors->first('age') }}</span>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-12 {{ $errors->has('blood_type_id') ? ' has-error' : '' }}">
                            <label for="blood_type_id">Grupa sanguina</label>
                            <select name="blood_type_id" id="blood_type_id" class="selectpicker">
                                @if (count($blood_types))
                                    @foreach($blood_types as $blood_type)
                                        <option value="{{ $blood_type->id }}" {{ isset($patient->medicalRecord) && $patient->medicalRecord->blood_type_id == $blood_type->id ? 'selected="selected"' : '' }}>{{ $blood_type->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if ($errors->has('blood_type_id'))
                                <span class="help-block">{{ $errors->first('blood_type_id') }}</span>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-12 {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone">Telefon</label>
                            <input id="phone" value="{{ $patient->phone }}" type="text" name="phone" class="form-control" placeholder="Telefon">
                            @if ($errors->has('phone'))
                                <span class="help-block">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <label for="sex">Sex</label>
                            <div class="radio">
                                <label for="male">
                                    Masculin
                                    <input id="male" type="radio" name="gender" value="m" {{ isset($patient->medicalRecord) && $patient->medicalRecord->gender == 'm' ? 'checked="checked"' : '' }}>
                                </label>
                                <label for="female">
                                    Feminin
                                    <input id="female" type="radio" name="gender" value="f" {{ isset($patient->medicalRecord) && $patient->medicalRecord->gender == 'f' ? 'checked="checked"' : '' }}>
                                </label>
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <div class="col-md-12 col-xs-12">
                            <button type="submit" class="btn btn-success">Salveaza</button>
                        </div>
                    </div>
                </form>

                <h4>Retele</h4>
                @if (count($patient->recipes))
                    @foreach ($patient->recipes as $recipe)
                        <div>
                            <p>Reteta # {{ $recipe->id . ' ' . $recipe->created_at }}</p>
                            <p>Diagnostic: {{ $recipe->diagnostic }}</p>
                            <p>Medicatie: {{ $recipe->medication }}</p>
                        </div>
                    @endforeach
                @else
                    Nu sunt retete
                @endif

                <h4>Analize</h4>
                @if (count($patient->labs))
                    @foreach($patient->labs as $lab)
                        <p>Data: {{ $lab->created_at }}</p>
                        <p>Rezultat: <a href="{{ URL::to('lab/show/' . $lab->id) }}" target="_blank">Vezi rezultat</a></p>
                    @endforeach
                @else
                    Nu sunt analize
                @endif
            </div>
        </div>
    </div>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
@endsection