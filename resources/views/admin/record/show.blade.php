@extends('admin.layouts.master')

@section('title')
    Vizualizare fisa {{ $patient->first_name . ' ' . $patient->last_name }}
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Vizualizare fisa {{ $patient->first_name . ' ' . $patient->last_name }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-container">
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('success') }}
                    </div>
                @elseif (Session::has('danger'))
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('danger') }}
                    </div>
                @endif


                    <div class="row">
                        <div class="col-md-6 col-xs-12 {{ $errors->has('cnp') ? ' has-error' : '' }}">
                            <label for="cnp">Cod Numeric Personal</label>
                            <p>{{ $patient->cnp }}</p>
                            @if ($errors->has('cnp'))
                                <span class="help-block">{{ $errors->first('cnp') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12  {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Adresa de email</label>
                            <p>{{ $patient->email }}</p>

                            @if ($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12 {{ $errors->has('dob') ? ' has-error' : '' }}">
                            <label for="dob">Data nasterii</label>
                            <p>{{$patient->medicalRecord->dob}}</p>

                            @if ($errors->has('dob'))
                                <span class="help-block">{{ $errors->first('dob') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12 {{ $errors->has('age') ? ' has-error' : '' }}">
                            <label for="age">Varsta</label>
                            <p>{{$patient->medicalRecord->age}}</p>
                            @if ($errors->has('age'))
                                <span class="help-block">{{ $errors->first('age') }}</span>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-12 {{ $errors->has('blood_type_id') ? ' has-error' : '' }}">
                            <label for="blood_type_id">Grupa sanguina</label>
                            <p>
                                @if (count($blood_types))
                                    @foreach($blood_types as $blood_type)
                                        {{ isset($patient->medicalRecord) && $patient->medicalRecord->blood_type_id == $blood_type->id ? $blood_type->name : '' }}
                                    @endforeach
                                @endif
                            </p>
                            @if ($errors->has('blood_type_id'))
                                <span class="help-block">{{ $errors->first('blood_type_id') }}</span>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-12 {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone">Telefon</label>
                            <p>{{ $patient->phone }}</p>
                            @if ($errors->has('phone'))
                                <span class="help-block">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <label for="sex">Sex:</label>
                            <div>{{isset($patient->medicalRecord) && $patient->medicalRecord->gender == 'm' ? 'Masculin' : 'Feminin'}}</div>
                        </div>

                    </div>


                <h4>Retele</h4>
                @if (count($patient->recipes))
                    @foreach ($patient->recipes as $recipe)
                        <div>
                            <p>Reteta # {{ $recipe->id . ' ' . $recipe->created_at }}</p>
                            <p>Diagnostic: {{ $recipe->diagnostic }}</p>
                            <p>Medicatie: {{ $recipe->medication }}</p>
                        </div>
                    @endforeach
                @else
                    Nu sunt retete
                @endif

                <h4>Analize</h4>
                @if (count($patient->labs))
                    @foreach($patient->labs as $lab)
                        <p>Data: {{ $lab->created_at }}</p>
                        <p>Rezultat: <a href="{{ URL::to('lab/show/' . $lab->id) }}" target="_blank">Vezi rezultat</a></p>
                    @endforeach
                @else
                    Nu sunt analize
                @endif
            </div>
        </div>
    </div>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
@endsection