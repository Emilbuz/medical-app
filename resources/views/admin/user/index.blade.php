@extends('admin.layouts.master')

@section('title')
    Utilizatori
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>{{ $role -> name }}</h3>
            @if (isset($users) && count($users))
                <div class="add-btn pull-right">
                    @if ($type != 'admin')
                        <a href="{{ URL::to('user/create/' . $type) }}" class="btn btn-success" id="create_user_btn">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    @endif
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="table-container">
                <div class="table-content">
                    <div class="row">
                        <div class="col-md-12">
                            @if (isset($users) && count($users))
                                <div class="table-responsive">
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="40">#ID</th>
                                                <th width="auto">Nume</th>
                                                <th width="55"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($users as $user)
                                            <tr>
                                                <td>{{ $user->id }}</td>
                                                <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                                                <td>
                                                    <a class="btn btn-primary btn-xs" href="{{ URL::to('user/show/' . $user->id) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <a class="btn btn-warning btn-xs" href="{{ URL::to('user/edit/' . $user->id) }}">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a class="btn btn-danger btn-xs" onclick="return confirm('Esti sigur ca vrei sa stergi acest utilizator?')" href="{{ URL::to('user/delete/' . $user->id) }}">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="text-center">
                                    <p>Nu exista niciun {{ strtolower($role->name) }}.</p>
                                    <a href="{{ URL::to('user/create/' . $type) }}" class="btn btn-success"><i class="fa fa-plus"></i> Adauga acum</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable();
        } );
    </script>
@endsection