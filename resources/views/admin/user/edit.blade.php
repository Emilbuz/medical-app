@extends('admin.layouts.master')

@section('title')
    Editare {{ $user->first_name . ' ' . $user->last_name }}
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Editare {{ $user->first_name . ' ' . $user->last_name }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('success') }}
                </div>
            @elseif (Session::has('danger'))
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('danger') }}
                </div>
            @endif
            <div class="box-container">
                <form action="{{ URL::to('user/update/' . $user->id) }}" method="POST">
                    <div class="row">
                        <div class="col-md-4 col-xs-12 {{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <input type="text" name="first_name" class="form-control" placeholder="Nume" value={{ $user->first_name }}>
                            @if ($errors->has('first_name'))
                                <span class="help-block">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-12 {{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <input type="text" name="last_name" class="form-control" placeholder="Prenume" value={{ $user->last_name }}>
                            @if ($errors->has('last_name'))
                                <span class="help-block">{{ $errors->first('last_name') }}</span>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" name="email" class="form-control" placeholder="Email" value={{ $user->email }}>
                            @if ($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        @if ($type == 'patient')
                            <div class="col-md-12 col-xs-12 {{ $errors->has('address') ? ' has-error' : '' }}">
                                <textarea name="address" class="form-control" placeholder="Adresa">{{ $user->address }}</textarea>
                                @if ($errors->has('address'))
                                    <span class="help-block">{{ $errors->first('address') }}</span>
                                @endif
                            </div>
                        @endif
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <div class="col-md-12 col-xs-12" >
                            <button type="submit" class="btn btn-success">Salveaza</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
@endsection