@extends('admin.layouts.master')

@section('title')
    Adauga {{ $role->name }}
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Adauga {{$role -> name}}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-container">
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('success') }}
                    </div>
                @elseif (Session::has('danger'))
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('danger') }}
                    </div>
                @endif
                <form action="{{ URL::to('user/save') }}" method="POST">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 {{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <input type="text" name="first_name" value="{{ Request::old('first_name') }}" class="form-control" placeholder="Nume">
                            @if ($errors->has('first_name'))
                                <span class="help-block">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12 {{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <input type="text" name="last_name" class="form-control" placeholder="Prenume">
                            @if ($errors->has('last_name'))
                                <span class="help-block">{{ $errors->first('last_name') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12 {{ $errors->has('cnp') ? ' has-error' : '' }}">
                            <input type="text" name="cnp" class="form-control" placeholder="Cod Numeric Personal">
                            @if ($errors->has('cnp'))
                                <span class="help-block">{{ $errors->first('cnp') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12  {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" name="email" value="{{ Request::old('email') }}" class="form-control" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12  {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12 {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <input type="text" name="phone" class="form-control" placeholder="Telefon">
                            @if ($errors->has('phone'))
                                <span class="help-block">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    @if ($type == 'patient')
                        <div class="col-xs-12  {{ $errors->has('address') ? ' has-error ' : '' }}">
                            <textarea class="form-control" name="address">Adresa</textarea>
                            @if ($errors->has('address'))
                                <span class="help-block">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    @endif
                    @if ($type == 'doctor')
                        <div class="col-md-6 col-xs-12  {{ $errors->has('$specialization') ? ' has-error ' : '' }}">
                            <select name="specialization" id="specialization" class="selectpicker">
                                <option value="0">Alege Specializare</option>
                                @if (count($specializations))
                                    @foreach ($specializations as $specialization)
                                        <option value="{{ $specialization->id }}">{{ $specialization->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if ($errors->has('specialization'))
                                <span class="help-block">{{ $errors->first('specialization') }}</span>
                            @endif
                        </div>
                    @endif

                    @if ($type == 'patient')
                            <div class="col-md-12 col-xs-12 mt-10 mb-20">
                                <h3>Adauga fisa medicala</h3>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="radio">
                                    <label for="male">
                                        Barbat
                                        <input id="male" type="radio" value="m"  name="record[gender]">
                                    </label>
                                    <label for="female">Femeie
                                    <input id="female" type="radio" value="f" name="record[gender]">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <select name="record[blood_type_id]" id="" class="selectpicker">
                                    @if (count($blood_types))
                                        @foreach($blood_types as $blood_type)
                                            <option value="{{ $blood_type->id }}">{{ $blood_type->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" name="record[birth_place]" class="form-control" placeholder="Locul nasterii">
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" name="record[dob]" id="datepicker" class="form-control" placeholder="Data nasterii">
                            </div>
                    @endif
                    <input type="hidden" name="type" value="{{ $role->slug }}">
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-success">Salveaza</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
@endsection