@extends('admin.layouts.master')

@section('title')
   Analize
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Adauga set de analize</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-container">
                <form action="{{ URL::to('lab/save') }}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 {{ $errors->has('dob') ? ' has-error ' : '' }}">
                            <input type="text" id="datepicker" name="dob" class="form-control" placeholder="Data Analizelor">
                            @if ($errors->has('dob'))
                                <span class="help-block">{{ $errors->first('dob') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" name="lab_name" placeholder="Numele analizelor">
                            {{--<textarea name="lab_name" id="lab_name" class="form-control" cols="20" rows="5" placeholder="Numele investigatiei"></textarea>--}}
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <textarea name="lab_observation" id="lab_observation" class="form-control" cols="20" rows="5" placeholder="Observatii"></textarea>
                        </div>
                        <div class="col-md-12 col-xs-12 pull-right">
                            <label for="lab_scan">Incarca scan</label>
                            <input type="file" name="lab_scan" id="lab_scan">
                        </div>
                        <input type="hidden" name="user_id" value="{{ $patient_id }}">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <div class="col-md-12 mt-20">
                            <button type="submit" class="btn btn-success">Salveaza</button>
                            <a class="btn btn-danger"  href="{{ URL::to('labs/' . $patient_id) }}">Anuleaza</a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker({
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
@endsection