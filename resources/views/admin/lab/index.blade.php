@extends('admin.layouts.master')

@section('title')
    Analize
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Analizele pacientului {{ $patient->first_name . ' ' . $patient->last_name }}</h3>
            @if (Auth::user()->is(['doctor', 'receptionist', 'admin']))
            <div class="add-btn pull-right">
                <a href="{{ URL::to('lab/create/' . $patient->id) }}" class="btn btn-success" id="create_lab_btn">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="table-container">
                <div class="table-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th width="auto">Nume</th>
                                        <th width="90">Data Analizelor</th>
                                        <th width="20"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (isset($patient->labs) && count($patient->labs))
                                        @foreach ($patient->labs as $lab)
                                            <tr>
                                                <td>{{ $lab->lab_name }}</td>
                                                <td>{{ $lab->created_at }}</td>
                                                <td>
                                                    <a class="btn btn-primary btn-xs" href="{{ URL::to('lab/show/' . $lab->id) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable();
        } );
    </script>
@endsection