@extends('admin.layouts.master')

@section('title')
    Analize
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Analize</h3>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="table-container">
                <div class="table-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th width="auto">Pacient</th>
                                        <th width="20"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (isset($patients) && count($patients))
                                        @foreach ($patients as $patient)
                                            <tr>
                                                <td>{{ $patient->first_name . ' ' . $patient->last_name }}</td>
                                                <td>
                                                     <a title="Vezi analizele" class="btn btn-primary btn-xs" href="{{ URL::to('labs/' . $patient->id) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable();
        } );
    </script>
@endsection