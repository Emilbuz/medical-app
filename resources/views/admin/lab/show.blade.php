@extends('admin.layouts.master')

@section('title')
    Vizualizare set #{{ $lab->id }}
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Vizualizare set #{{ $lab->id }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-container">
                <div class="">
                    <span class="appointment-label">Data:</span> <span>{{ $lab->created_at }}</span>
                </div>
                <div class="">
                    <span class="appointment-label">Pacient:</span> <span> {{ $lab->user->first_name  . ' ' . $lab->user->last_name }}</span>
                </div>
                <div class="">
                    <span class="appointment-label">Nume set:</span> <span> {{ $lab->lab_name }}</span>
                </div>
                <div class="">
                    <img src="{{ asset('scan/' . $lab->lab_scan) }}" alt="">
                </div>

            </div>
        </div>
    </div>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
@endsection