@extends('admin.layouts.master')

@section('title')
    <?php $title = $appointment->status == 'done' ? 'Consult' : 'Programare'; ?>
    Vizualizare {{ $title }} # {{ $appointment->id }}
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Vizualizare {{ $title }} # {{ $appointment->id }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-container">
                <div class="">
                    <span class="appointment-label">Data:</span> <span>{{ $appointment->date . ' ' . $appointment->time }}</span>
                </div>
                <div class="">
                    <span class="appointment-label">Pacient:</span> <span> {{ $appointment->user->first_name  . ' ' . $appointment->user->last_name }}</span>
                </div>
                {{--@if ($appointment->status == 'done' && Auth::user()->is('doctor'))--}}
                {{--@if (isset($appointment->recipe))--}}
                {{--<a class="btn btn-primary" href="{{ URL::to('recipe/show/' . $appointment->recipe->id ) }}">vezi reteta</a>--}}
                {{--@else--}}
                {{--<a class="btn btn-success" href="{{ URL::to('recipe/create/' . $appointment->id ) }}">Adauga reteta</a>--}}
                {{--@endif--}}
                {{--@endif--}}
            </div>
        </div>
    </div>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
@endsection