@extends('admin.layouts.master')

@section('title')
    Editare programare # {{ $appointment->id }}
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Editeaza Programarea {{ $appointment->id }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-container">
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('success') }}
                    </div>
                @elseif (Session::has('danger'))
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('danger') }}
                    </div>
                @endif
                <form action="{{ URL::to('appointment/update/' . $appointment->id) }}" method="POST">
                    <div class="row">
                        <div class="col-md-3 col-xs-12 {{ $errors->has('doctor_id') ? ' has-error' : '' }}">
                            <select name="doctor_id" id="doctor_id" class="selectpicker">
                                <option value="0">Alege doctor</option>
                                @if (count($doctors))
                                    @foreach ($doctors as $doctor)
                                        <option value="{{ $doctor->id }}" {{ $appointment->doctor_id == $doctor->id ? 'selected="selected"' : '' }}>
                                            {{ $doctor->first_name }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                            @if ($errors->has('doctor_id'))
                                <span class="help-block">{{ $errors->first('doctor_id') }}</span>
                            @endif
                        </div>
                        <div class="col-md-3 col-xs-12 {{ $errors->has('date') ? ' has-error' : '' }}">
                            <input type="text" id="datepicker" class="form-control" name="date" value="{{ $appointment->date }}" placeholder="Data">
                            @if ($errors->has('date'))
                                <span class="help-block">{{ $errors->first('date') }}</span>
                            @endif
                        </div>
                        <div class="col-md-3 col-xs-12 {{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <input type="time" name="time" class="form-control" placeholder="Ora" value="{{ $appointment->time }}">
                            @if ($errors->has('first_name'))
                                <span class="help-block">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                        <div class="col-md-3 col-xs-12 {{ $errors->has('status') ? ' has-error' : '' }}">
                            <select name="status" id="status" class="selectpicker">
                                <option {{ $appointment->status == 'active' ? 'selected="selected"' : '' }} value="active">Activ</option>
                                <option {{ $appointment->status == 'canceled' ? 'selected="selected"' : '' }} value="canceled">Anulat</option>
                                <option {{ $appointment->status == 'inactive' ? 'selected="selected"' : '' }} value="inactive">Inactiv</option>
                                <option {{ $appointment->status == 're' ? 'selected="selected"' : '' }} value="re">Reprogramare</option>
                                <option {{ $appointment->status == 'done' ? 'selected="selected"' : '' }} value="done">Finalizata</option>
                            </select>
                            @if ($errors->has('status'))
                                <span class="help-block">{{ $errors->first('status') }}</span>
                            @endif
                        </div>
                        <div class="col-md-12 col-xs-12 {{ $errors->has('description') ? ' has-error' : '' }}">
                            <textarea name="description" class="form-control" id="description" cols="30" rows="10" placeholder="Observatii">{{ $appointment->description }}</textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">{{ $errors->first('description') }}</span>
                            @endif
                        </div>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <div class="col-md-12 col-xs-12">
                            <button type="submit" class="btn btn-success">Salveaza</button>
                            <a class="btn btn-danger" href="{{ URL::to('appointments') }}">Anuleaza</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
@endsection