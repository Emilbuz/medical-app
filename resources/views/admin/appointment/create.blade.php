@extends('admin.layouts.master')

@section('title')
    Programari
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Creeaza Programare</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-container">
                @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('success') }}
                    </div>
                @elseif (Session::has('danger'))
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('danger') }}
                    </div>
                @endif
                <form action="{{ URL::to('appointment/save') }}" method="POST">
                    <div class="row">
                        @if(!Auth::user() -> is("patient"))
                        <div class="col-md-6 col-xs-12 {{ $errors->has('user_id') ? ' has-error' : '' }}">
                            <select name="user_id" id="user_id" class="selectpicker">
                                <option value="0">Alege pacient</option>
                                @if (count($patients))
                                    @foreach ($patients as $patient)
                                        <option value="{{ $patient->id }}">{{ $patient->first_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if ($errors->has('user_id'))
                                <span class="help-block">{{ $errors->first('user_id') }}</span>
                            @endif
                        </div>
                        @endif
                        <div class="col-md-6 col-xs-12 {{ $errors->has('doctor_id') ? ' has-error' : '' }}">
                            <select name="doctor_id" id="doctor_id" class="selectpicker">
                                <option value="0">Alege doctor</option>
                                @if (count($doctors))
                                    @foreach ($doctors as $doctor)
                                        <option value="{{ $doctor->id }}">{{ $doctor->first_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if ($errors->has('doctor_id'))
                                <span class="help-block">{{ $errors->first('doctor_id') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12 {{ $errors->has('datepicker') ? ' has-error' : '' }}">
                            <input type="text" id="datepicker" class="form-control" name="date" placeholder="Data">
                            @if ($errors->has('datepicker'))
                                <span class="help-block">{{ $errors->first('datepicker') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 col-xs-12 {{ $errors->has('time') ? ' has-error' : '' }}">
                            <input type="time" name="time" step="30" class="form-control" placeholder="Ora">
                            @if ($errors->has('time'))
                                <span class="help-block">{{ $errors->first('time') }}</span>
                            @endif
                        </div>
                        <div class="col-md-12 col-xs-12 {{ $errors->has('description') ? ' has-error' : '' }}">
                            <textarea name="description" id="description" class="form-control" cols="30" rows="10" placeholder="Observatii"></textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">{{ $errors->first('description') }}</span>
                            @endif
                        </div>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Salveaza</button>
                            <a class="btn btn-danger" href="{{ Url::to('appointments') }}">Anuleaza</a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
@endsection