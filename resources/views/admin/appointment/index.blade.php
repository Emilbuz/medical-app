@extends('admin.layouts.master')

@section('title')
    Programari
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Programari</h3>
            <div class="add-btn pull-right">
                <a href="{{ URL::to('appointment/create') }}" class="btn btn-success" id="create_user_btn">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="table-container">
                <div class="table-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th width="40">#ID</th>
                                            <th>Pacient</th>
                                            <th>Doctor</th>
                                            <th>Data</th>
                                            <th>Status</th>
                                            <th width="55"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if (isset($appointments) && count($appointments))
                                        @foreach ($appointments as $appointment)
                                            <?php

                                                switch ($appointment->status) {
                                                    case 'active':
                                                        $status = 'Activ';
                                                        break;
                                                    case 'inactive':
                                                        $status = 'Inactiv';
                                                        break;
                                                    case 'canceled':
                                                        $status = 'Anulat';
                                                        break;
                                                    case 're':
                                                        $status = 'Reprogramat';
                                                        break;
                                                    case 'done':
                                                        $status = 'Finalizat';
                                                        break;
                                                    default:
                                                        $status = 'Activ';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{ $appointment->id }}</td>
                                                <td>{{ isset($patients[$appointment->user_id]) ? $patients[$appointment->user_id]['first_name'] : ''}}</td>
                                                <td>{{ isset($doctors[$appointment->doctor_id]) ? $doctors[$appointment->doctor_id]['first_name'] : '' }}</td>
                                                <td>{{ $appointment->date . ' ' . $appointment->time }}</td>
                                                <td>{{ $status }}</td>
                                                <td>
                                                    <a class="btn btn-primary btn-xs" href="{{ URL::to('appointment/show/' . $appointment->id) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <a class="btn btn-warning btn-xs" href="{{ URL::to('appointment/edit/' . $appointment->id) }}">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a class="btn btn-danger btn-xs" href="{{ URL::to('appointment/delete/' . $appointment->id) }}">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable();
        } );
    </script>
@endsection