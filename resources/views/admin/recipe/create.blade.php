@extends('admin.layouts.master')

@section('title')
    Adauga reteta
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Adauga Reteta</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-container">
                <form action="{{ URL::to('recipe/save') }}" method="POST">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <textarea name="diagnostic" id="diagnostic" class="form-control" cols="20" rows="5" placeholder="Diagnostic"></textarea>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <textarea name="medication" id="medication" class="form-control" cols="20" rows="5" placeholder="Medicatie"></textarea>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <textarea name="obs" id="obs" class="form-control" cols="20" rows="5" placeholder="Observatii"></textarea>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <input type="text" class="form-control" name="dose" placeholder="Doza">
                        </div>
                        <div class="col-md-6 col-xs-12">
                            Este compensata
                            <input type="checkbox" name="compensated" value="1">
                        </div>
                        <input type="hidden" name="user_id" value="{{ $patient_id }}">
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Salveaza</button>
                            <a class="btn btn-danger" href="{{ URL::to('patients_recipe') }}">Anuleaza</a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection