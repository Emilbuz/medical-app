@extends('admin.layouts.master')

@section('title')
    Retete
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Retetele pacientului {{ $patient->first_name . ' ' . $patient->last_name }}</h3>
            @if (Auth::user()->is(['admin', 'doctor']))
            <div class="add-btn pull-right">
                <a href="{{ URL::to('recipe/create/' . $patient->id ) }}" class="btn btn-success" id="create_user_btn">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
            </div>
            @endif
        </div>

    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="table-container">
                <div class="table-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th width="90">Data</th>
                                        <th width="auto">Diagnostic</th>
                                        <th width="20"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (isset($patient->recipes) && count($patient->recipes))
                                        @foreach ($patient->recipes as $recipe)
                                            <tr>
                                                <td>{{ $recipe->created_at }}</td>
                                                <td>{{ $recipe->diagnostic }}</td>
                                                <td>
                                                    <a class="btn btn-primary btn-xs" href="{{ URL::to('recipe/show/' . $recipe->id) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable();
        } );
    </script>
@endsection