@extends('admin.layouts.master')

@section('title')
    Reteta
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Vizualizare Reteta</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label for="">Diagnostic:</label> {{ $recipe->diagnostic }}
        </div>
        <div class="col-md-12">
            <label>Medicatie: </label>{{ $recipe->medication . ' - ' . $recipe->dose }}
        </div>
        <div class="col-md-12">
            <label>Compensata:</label>{{ $recipe->compensated ? 'Da' : 'Nu' }}
        </div>
        <div class="col-md-12">
            <a class="btn btn-primary" href="{{ URL::to('patients_recipe') }}">Inapoi</a>
        </div>
    </div>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
@endsection