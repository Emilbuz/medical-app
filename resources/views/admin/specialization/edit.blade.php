@extends('admin.layouts.master')

@section('title')
    Editare specializare
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Editare specializare # {{ $specialization->id }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('success'))
                <div class="alert alert-success alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('success') }}
                </div>
            @elseif (Session::has('danger'))
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('danger') }}
                </div>
            @endif
            <div class="box-container">
                <form action="{{ URL::to('specialization/update/' . $specialization->id) }}" method="POST">
                    <div class="row">
                        <div class="col-md-4 col-xs-12 {{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" name="name" class="form-control" value="{{ $specialization->name }}" placeholder="Nume Specializare">
                            @if ($errors->has('name'))
                                <span class="help-block">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-12 {{ $errors->has('code') ? ' has-error' : '' }}">
                            <input type="text" name="code" class="form-control" placeholder="Cod Specializare" value="{{ $specialization->code }}">
                            @if ($errors->has('code'))
                                <span class="help-block">{{ $errors->first('code') }}</span>
                            @endif
                        </div>
                        <div class="col-md-4 col-xs-12 {{ $errors->has('status') ? ' has-error' : '' }}">
                            <select name="status" id="status" class="selectpicker">
                                <option {{ $specialization->status == 1 ? 'selected="selected"' : '' }} value="1">Activ</option>
                                <option {{ $specialization->status == 0 ? 'selected="selected"' : '' }} value="0">Inactiv</option>
                            </select>
                            @if ($errors->has('status'))
                                <span class="help-block">{{ $errors->first('status') }}</span>
                            @endif
                        </div>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                        <div class="col-md-12 col-xs-12">
                            <button class="btn btn-success" type="submit">Salveaza</button>
                            <a class="btn btn-danger" href="{{ URL::to('specializations') }}">Anuleaza</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection