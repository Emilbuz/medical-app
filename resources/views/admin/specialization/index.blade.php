@extends('admin.layouts.master')

@section('title')
    Specializari
@endsection

@section('content')
    <div class="row">
        <div class="page-header-custom">
            <h3>Specializari</h3>
            <div class="add-btn pull-right">
                <a href="{{ URL::to('specialization/create') }}" class="btn btn-success" id="create_user_btn">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="table-container">
                <div class="table-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th width="40">#ID</th>
                                            <th>Nume</th>
                                            <th>Cod</th>
                                            <th>Status</th>
                                            <th width="40"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if (isset($specializations) && count($specializations))
                                        @foreach ($specializations as $specialization)
                                            <?php

                                            switch ($specialization->status) {
                                                case 1:
                                                    $status = 'Activ';
                                                    break;
                                                case '0':
                                                    $status = 'Inactiv';
                                                    break;
                                                default:
                                                    $status = 'Activ';
                                            }
                                            ?>
                                            <tr>
                                                <td>{{ $specialization->id }}</td>
                                                <td>{{ $specialization->name }}</td>
                                                <td>{{ $specialization->code }}</td>
                                                <td>{{ $status }}</td>
                                                <td>
                                                    <a class="btn btn-warning btn-xs" href="{{ URL::to('specialization/edit/' . $specialization->id) }}">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a onclick="return confirm('Esti sigur ca vrei sa stergi aceasta specializare?')" class="btn btn-danger btn-xs" href="{{ URL::to('specialization/delete/' . $specialization->id) }}">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable();
        } );
    </script>
@endsection