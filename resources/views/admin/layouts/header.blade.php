<header>
    <div class="header-wrapper">
        <div class="logo-wrapper">
            <a href="{{ URL::to('/') }}">
                Cabinet<strong>Medical</strong>
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset('/img/dummy_avatar.jpg') }}" alt="">
                    <span>{{Auth::user() -> first_name}} {{Auth::user() -> last_name}}</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#">Page 1-1</a></li>
                    <li><a href="#">Page 1-2</a></li>
                    <li><a href="#">Page 1-3</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ URL::to('logout') }}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i>
                </a>
            </li>
        </ul>
    </div>
</header>