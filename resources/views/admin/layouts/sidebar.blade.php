<?php
    if(Auth::user() -> is('patient')) {
        $recipe_url = "recipes/". Auth::user() -> id;
        $labs_url =  "labs/". Auth::user() -> id;
        $record_url = "medical_record/" . Auth::user() -> id;
        $record_label = "Fisa Medicala";
    }else{
        $recipe_url = "patients_recipe";
        $labs_url = "patients_labs";
        $record_url = "medical_records";
        $record_label = "Fise Pacienti";
    }
?>
<section class="sidebar-nav">
    <div class="sidebar-header">
        fd
    </div>
    <div class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="visible-xs navbar-brand">Meniu</span>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
            <ul class="nav navbar-nav">


                @if (Auth::user() -> is('receptionist'))
                <li><a href="{{ URL::to('users/patient') }}">Pacienti</a></li>
                @endif
                <li><a href="{{ URL::to($record_url) }}">{{ $record_label }}</a></li>
                <li><a href="{{ URL::to('appointments')  }}">Programari</a></li>
                @if (!Auth::user()->is(['patient', 'doctor']))
                    @if (!Auth::user() -> is('receptionist'))
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="">
                            Utilizatori
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                                <li><a href="{{ URL::to('users/receptionist') }}">Receptionist</a></li>
                                <li><a href="{{ URL::to('users/doctor') }}">Doctori</a></li>

                            <li><a href="{{ URL::to('users/patient') }}">Pacienti</a></li>
                        </ul>
                    </li>
                    @endif
                    @if (Auth::user() -> is('admin'))
                        <li><a href="{{ URL::to('specializations') }}">Specializari Medici</a></li>
                    @endif
                @endif
                <li><a href="{{ URL::to($recipe_url) }}">Retete</a></li>
                <li><a href="{{ URL::to($labs_url) }}">Analize</a></li>

            </ul>
        </div>
    </div>
</section>
