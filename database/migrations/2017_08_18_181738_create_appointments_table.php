<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('doctor_id');
            $table->string('date');
            $table->string('time');
            $table->text('description');
            $table->string('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('appointments');
    }
}
