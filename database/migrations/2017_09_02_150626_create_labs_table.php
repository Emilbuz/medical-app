<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabsTable extends Migration
{
    public function up()
    {
        Schema::create('labs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->date('date');
            $table->string('lab_name');
            $table->string('lab_scan');
            $table->text('lab_observation');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('labs');
    }
}
