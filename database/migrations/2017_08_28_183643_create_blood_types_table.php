<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBloodTypesTable extends Migration
{
    public function up()
    {
        Schema::create('blood_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('sign');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('blood_types');
    }
}
