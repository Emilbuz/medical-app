<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalRecordsTable extends Migration
{
    public function up()
    {
        Schema::create('medical_records', function (Blueprint $table) {
            $table->increments('id');
            $table->date('dob');
            $table->integer('user_id');
            $table->integer('modified_by');
            $table->integer('created_by');
            $table->string('blood_type_id');
            $table->string('gender');
            $table->text('birth_place');
            $table->string('age');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('medical_records');
    }
}
