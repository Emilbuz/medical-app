<?php

use Illuminate\Database\Seeder;
use Bican\Roles\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $adminRole = Role::create([
            'name' => 'Administrator',
            'slug' => 'admin',
        ]);

        $receptionistRole = Role::create([
            'name' => 'Reprezentant Cabinet',
            'slug' => 'receptionist',
        ]);

        $doctorRole = Role::create([
            'name' => 'Doctor',
            'slug' => 'doctor',
        ]);

        $patientRole = Role::create([
            'name' => 'Pacient',
            'slug' => 'patient',
        ]);

        $user = User::create([
            'email'     => 'admin@admin.com',
            'password'  => Hash::make('password')
        ]);

        $user->attachRole($adminRole);
    }
}
