<?php

use Illuminate\Database\Seeder;
use App\BloodType;

class BloodTypesSeeder extends Seeder
{
    public function run()
    {
        $blood_types = [
            ['name' => 'O', 'sign' => 'I'],
            ['name' => 'A', 'sign' => 'II'],
            ['name' => 'B', 'sign' => 'III'],
            ['name' => 'AB', 'sign' => 'IV'],
        ];

        foreach ($blood_types as $blood_type) {
            BloodType::create($blood_type);
        }
    }
}
