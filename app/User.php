<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

class User extends Authenticatable implements HasRoleAndPermissionContract
{
    use HasRoleAndPermission;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'dob', 'age', 'address', 'gender'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = true;

    # Relations

    public function appointments()
    {
        return $this -> hasMany('App\Appointment');
    }

    public function specialization()
    {
        return $this->belongsTo('App\Specialization');
    }

    public function bloodType()
    {
        return $this->belongsTo('App\BloodType');
    }

    public function recipes()
    {
        return $this -> hasMany('App\Recipe');
    }

    public function labs()
    {
        return $this -> hasMany('App\Lab');
    }

    public function medicalRecord()
    {
        return $this->hasOne('App\MedicalRecord');
    }

    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute)
        {
            parent::setAttribute($key, $value);
        }
    }
}
