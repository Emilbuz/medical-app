<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = ['user_id', 'diagnostic', 'medication', 'dose', 'compensated', 'status'];

    public $timestamps = true;

    public function appointment()
    {
        return $this->belongsTo('App\Appointment');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
