<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BloodType extends Model
{
    protected $fillable = ['name', 'sign'];

    public $timestamps = true;

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
