<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = ['user_id', 'doctor_id', 'date', 'time', 'description', 'status'];

    public $timestamp = true;

    public function user()
    {
        return $this -> belongsTo('App\User');
    }

    public function recipe()
    {
        return $this->hasOne('App\Recipe');
    }
}
