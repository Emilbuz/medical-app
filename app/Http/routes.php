<?php

Route::group(['middlewareGroups' => ['web']], function () {

    Route::get('login', 'Admin\UserController@login_form');
    Route::post('login', 'Admin\UserController@login');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', 'Admin\UserController@dashboard');

        # Utilizatori
        Route::get('patient/profile/{id}', 'Admin\UserController@profile');
        Route::get('users/{type}', 'Admin\UserController@index');
        Route::get('user/create/{type}', 'Admin\UserController@create');
        Route::post('user/save', 'Admin\UserController@save');
        Route::get('user/edit/{id}', 'Admin\UserController@edit');
        Route::post('user/update/{id}', 'Admin\UserController@update');
        Route::get('user/delete/{id}', 'Admin\UserController@delete');

        Route::get('logout', 'Admin\UserController@logout');

        # Programari

        Route::get('appointments', 'Admin\AppointmentController@index');
        Route::get('appointment/create', 'Admin\AppointmentController@create');
        Route::post('appointment/save', 'Admin\AppointmentController@save');
        Route::get('appointment/edit/{id}', 'Admin\AppointmentController@edit');
        Route::post('appointment/update/{id}', 'Admin\AppointmentController@update');
        Route::get('appointment/delete/{id}', 'Admin\AppointmentController@delete');
        Route::get('appointment/show/{id}', 'Admin\AppointmentController@show');

        # Specializari

        Route::get('specializations', 'Admin\SpecializationController@index');
        Route::get('specialization/create', 'Admin\SpecializationController@create');
        Route::post('specialization/save', 'Admin\SpecializationController@save');
        Route::get('specialization/edit/{id}', 'Admin\SpecializationController@edit');
        Route::post('specialization/update/{id}', 'Admin\SpecializationController@update');
        Route::get('specialization/delete/{id}', 'Admin\SpecializationController@delete');

        # Reteta

        Route::get('recipes/{patient_id}', 'Admin\RecipeController@index');
        Route::get('patients_recipe', 'Admin\RecipeController@patients');
        Route::get('recipe/create/{patient_id}', 'Admin\RecipeController@create');
        Route::post('recipe/save', 'Admin\RecipeController@save');
        Route::get('recipe/show/{id}', 'Admin\RecipeController@show');

        #Analize

        Route::get('patients_labs', 'Admin\LabController@patients');
        Route::post('lab/save', 'Admin\LabController@save');
        Route::get('lab/show/{id}', 'Admin\LabController@show');
        Route::get('labs/{patient_id}', 'Admin\LabController@index');
        Route::get('lab/create/{patient_id}', 'Admin\LabController@create');

        #Fisa

        Route::get('medical_record/{user_id}', 'Admin\MedicalRecordController@edit');
        Route::get('medical_records', 'Admin\MedicalRecordController@patients');
        Route::post('medical_record/update/{user_id}', 'Admin\MedicalRecordController@update');
    }); // auth middleware
}); // web middleware


