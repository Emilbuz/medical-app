<?php

namespace App\Http\Controllers\Admin;

use App\Recipe;
use App\User;
use Illuminate\Support\Facades\Auth;
use Bican\Roles\Models\Role;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Appointment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{
    public function index()
    {
        if(Auth::user() -> is("patient")){
            $appointments = DB::table('appointments')->where(["user_id" => Auth::user() -> id ] )->orderBy('created_at', 'ASC')->get();
        }else{
            $appointments = DB::table('appointments')->where([])->orderBy('created_at', 'ASC')->get();
        }

        $patients = User::whereHas('roles', function ($q) {
            $q->where('slug', 'patient');
        })->get()->toArray();
        $patients = array_column($patients, null, 'id');

        $doctors = User::whereHas('roles', function ($q) {
            $q->where('slug', 'doctor');
        })->get()->toArray();
        $doctors = array_column($doctors, null, 'id');

        return view('admin.appointment.index', compact('appointments', 'patients', 'doctors'));
    }

    public function create()
    {
        $patients = User::whereHas('roles', function ($q) {
            $q->where('slug', 'patient');
        })->get();

        $doctors = User::whereHas('roles', function ($q) {
            $q->where('slug', 'doctor');
        })->get();

        return view('admin.appointment.create', compact('patients', 'doctors'));
    }

    public function save(Request $request)
    {
        $data = $request->all();

        if(!isset($data['user_id'])){
            $data['user_id'] = Auth::user() -> id;
        }

        $validator = Validator::make($data, [
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('appointment/create')->withErrors($validator)->withInput();
        }
        $data['status'] = 'active';



        $appointment = Appointment::create($data);

        if ($appointment) {
            Session::flash('success', 'Programare adaugata!');
            return redirect('appointments');
        }

    }

    public function show($id)
    {
        $appointment = Appointment::find($id);

        return view('admin.appointment.show', compact('appointment'));
    }

    public function edit($id)
    {
        $appointment = null;

        if (is_numeric($id)) {
            $appointment = Appointment::find($id);
        }

        $doctors = User::whereHas('roles', function ($q) {
            $q->where('slug', 'doctor');
        })->get();

        if ($appointment) {
            return view('admin.appointment.edit', compact('appointment', 'doctors'));
        }

        Session::flash('danger', 'Programare invalida! Reincarca pagina!');
        return redirect()->back();
    }

    public function update($id, Request $request)
    {
        $appointment = null;

        if (is_numeric($id)) {
            $appointment = Appointment::find($id);
        }

        $data = $request->all();

        if ($appointment) {
            $appointment->update($data);

            Session::flash('success', 'Programare actualizata!');
            return redirect('appointment/edit/' . $id);
        }
    }

    public function delete($id)
    {
        $appointment = null;

        if (is_numeric($id)) {
            $appointment = Appointment::find($id);
        }

        if ($appointment) {
//            $recipe = Recipe::where('appointment_id', $id)->first();
//            if ($recipe) {
//                $recipe->destroy($recipe->id);
//            }

            $appointment->destroy($id);

            Session::flash('success', 'Consultatie stearsa');
            return redirect('appointments');
        }
    }
}
