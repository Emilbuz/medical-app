<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Lab;
use App\Http\Requests;
use Intervention\Image\Facades\Image;

class LabController extends Controller
{
    public function index($patient_id = null)
    {
        $patient = User::find($patient_id);

        return view('admin.lab.index', compact('patient'));
    }

    public function patients()
    {
        $patients = User::whereHas('roles', function ($q) {
            $q->where('slug', 'patient');
        })->get();

        return view('admin.lab.patient_labs', compact('patients'));
    }

    public function create($patient_id)
    {
        return view('admin.lab.create', compact('patient_id'));
    }

    public function save(Request $request)
    {
        ini_set('memory_limit','256M');
        $data = $request->all();

        $img = Image::make($data['lab_scan']);

        if (!file_exists(public_path('scan'))) {
            mkdir(public_path('scan'));
        }

        $data['lab_scan'] = 'lab_' . $data['user_id'] . '_' . time() . '.png';

        $img->save(public_path('scan/' . $data['lab_scan']));

        Lab::create($data);

        return redirect('labs/' . $data['user_id']);
    }

    public function show($id)
    {
        $lab = Lab::find($id);

        return view('admin.lab.show', compact('lab'));
    }
}
