<?php

namespace App\Http\Controllers\Admin;

use App\Specialization;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class SpecializationController extends Controller
{
    public function index()
    {
        $specializations = Specialization::all();

        return view('admin.specialization.index', compact('specializations'));
    }

    public function create()
    {
        return view('admin.specialization.create');
    }

    public function save(Request $request)
    {
        $data = $request->all();

        Specialization::create($data);

        Session::flash('success', 'Specializare adaugata!');
        return redirect('specializations');
    }

    public function edit($id)
    {
        if (!is_numeric($id)) {
            Session::flash('danger', 'Invalid ID');
            return redirect('specializations');
        }
        $specialization = Specialization::find($id);

        return view('admin.specialization.edit', compact('specialization'));
    }

    public function update($id, Request $request)
    {
        if (!is_numeric($id)) {
            Session::flash('danger', 'Invalid ID');
            return redirect('specializations');
        }
        $specialization = Specialization::find($id);

        $data = $request->all();

        $specialization->update($data);
        Session::flash('success', 'Specializare actualizata!');
        return redirect('specializations');
    }

    public function delete($id)
    {
        if (!is_numeric($id)) {
            Session::flash('danger', 'Invalid ID');
            return redirect('specializations');
        }
        $specialization = Specialization::find($id);

        $specialization->destroy($id);
        Session::flash('success', 'Specializare stearsa!');
        return redirect('specializations');
    }
}
