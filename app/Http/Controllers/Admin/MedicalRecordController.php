<?php

namespace App\Http\Controllers\Admin;

use App\BloodType;
use App\MedicalRecord;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MedicalRecordController extends Controller
{
    public function edit($user_id = null)
    {
        if (!$user_id) {
            $user_id = Auth::user()->id;
        }

        $patient = User::find($user_id);

        if (!Auth::user()->is('receptionist')) {
            $blood_types = BloodType::all();
            return view('admin.record.show', compact('patient','blood_types'));
        } else {
            $blood_types = BloodType::all();

            return view('admin.record.edit', compact('patient', 'blood_types'));
        }
    }

    public function patients()
    {
        $patients = User::whereHas('roles', function ($q) {
            $q->where('slug', 'patient');
        })->get();

        return view('admin.record.patient_records', compact('patients'));
    }

    public function update($id, Request $request)
    {
        if (!is_numeric($id)) {
            Session::flash('danger', 'Invalid ID');
            return redirect('medical_records');
        }
        $record = MedicalRecord::find($id);

        $data = $request->all();

        $record->update($data);
        Session::flash('success', 'Fisa medicala actualizata!');
        return redirect('medical_records');
    }
}
