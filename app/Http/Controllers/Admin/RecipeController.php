<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Recipe;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class RecipeController extends Controller
{
    public function index($patient_id = null)
    {
        $patient = User::find($patient_id);

        return view('admin.recipe.index', compact('patient'));
    }

    public function patients()
    {
        $patients = User::whereHas('roles', function ($q) {
            $q->where('slug', 'patient');
        })->get();

        return view('admin.recipe.patient_recipes', compact('patients'));
    }

    public function create($patient_id)
    {
        return view('admin.recipe.create', compact('patient_id'));
    }

    public function save(Request $request)
    {
        $data = $request->all();

        Recipe::create($data);

        return redirect('patients_recipe');
    }

    public function show($id)
    {
        $recipe = Recipe::find($id);
        return view('admin.recipe.show', compact('recipe'));
    }
}
