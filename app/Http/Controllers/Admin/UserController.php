<?php

namespace App\Http\Controllers\Admin;

use App\BloodType;
use App\MedicalRecord;
use App\Specialization;
use App\User;
use Bican\Roles\Models\Role;
use Illuminate\Http\Request;
use DateTime;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function dashboard()
    {
        return view('dashboard');
    }

    public function index($type)
    {
        $types = ['admin', 'patient', 'doctor', 'receptionist'];
        if (!in_array($type, $types)) {
            Session::flash('danger', 'Tip de utilizator invalid!');
            return redirect()->back();
        }

        $role = Role::where('slug', $type)->first();

        $role_users = DB::table('role_user')->where('role_id', $role->id)->get();
        $user_ids = array_column($role_users, 'user_id');

        $users = User::whereIn('id', $user_ids)->get();

        return view('admin.user.index', compact('users', 'type', 'role'));
    }

    public function create($type)
    {
        $types = ['patient', 'doctor', 'receptionist'];
        if (!in_array($type, $types)) {
            Session::flash('danger', 'Tip de utilizator invalid!');
            return redirect()->back();
        }

        $role = Role::where('slug', $type)->first();

        $specializations = Specialization::where('status', 1)->get();
        $blood_types = BloodType::all();

        return view('admin.user.create', compact('type', 'role', 'specializations', 'blood_types'));
    }

    public function save(Request $request)
    {
        $data = $request->all();

        $validation_rules = [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'email'             => 'required|unique:users',
            'password'          => 'required|min:6',
        ];

        if ($data['type'] == 'patient') {
            $validation_rules['cnp'] = 'required|min:13|max:13';
        }

        //$data['dob'] = date(strtotime('Y-m-d', $data['dob']));

        $validator = Validator::make($data, $validation_rules);

        if ($validator->fails()) {
            return redirect('user/create/' . $data['type'])->withErrors($validator)->withInput();
        }

        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        $role = Role::where('slug', $data['type'])->first();

        if ($role->slug == 'patient') {
            if (isset($data['record'])) {
                $record['blood_type_id'] = $data['record']['blood_type_id'];
                $record['gender'] = $data['record']['gender'];
                $record['dob'] = $data['record']['dob'];
                $record['user_id'] = $user->id;
                $record['created_by'] = Auth::user()->id;
                $record['birth_place'] = $data['record']['birth_place'];

                $from = new DateTime($data['record']['dob']);
                $to   = new DateTime('today');
                $age = $from->diff($to)->y;

                $record['age'] = $age;

                MedicalRecord::create($record);
            }
        }

        $user->attachRole($role->id);

        return redirect('users/' . $role->slug);
    }

    public function edit($id)
    {
        if (!is_numeric($id)) {
            Session::flash('danger', 'Invalid ID!');
            return redirect()->back();
        }

        $user = User::find($id);
        $role_users = DB::table('role_user')->where('user_id', $user->id)->first();
        $role = Role::find($role_users->role_id);
        $type = $role->slug;

        if ($user) {
            return view('admin.user.edit', compact('user', 'type'));
        }
    }

    public function update($id, Request $request)
    {
        $data = $request->all();

        if (!is_numeric($id)) {
            Session::flash('danger', 'Invalid ID!');
            return redirect()->back();
        }

        //$data['dob'] = date(strtotime('Y-m-d', $data['dob']));

        $user = User::find($id);
        $role_users = DB::table('role_user')->where('user_id', $user->id)->first();
        $role = Role::find($role_users->role_id);

        $user->update($data);

        return redirect('users/' . $role->slug);
    }

    public function delete($id)
    {
        if (!is_numeric($id)) {
            Session::flash('danger', 'Invalid ID!');
            return redirect()->back();
        }

        $user = User::find($id);
        if ($user) {
            $role_users = DB::table('role_user')->where('user_id', $user->id)->first();
            $role = Role::find($role_users->role_id);

            $user->detachAllRoles();
            $user->destroy($id);

            Session::flash('success', 'Utilizator sters!');
            return redirect('users/' . $role->slug);
        }

        Session::flash('danger', 'Utilizator invalid!');
        return redirect()->back();
    }

    public function login_form()
    {
        return view('admin/login');
    }

    public function login(Request $request)
    {
        $data = $request->only(['email', 'password']);

        if (!Auth::check()) {
            if (Auth::attempt($data)) {
                return redirect('/');
            }

            Session::flash('danger', 'Emailul sau parola nu sunt corecte!');
            return redirect('login');
        }

        return redirect('/');
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }

        return redirect('login');
    }
}
