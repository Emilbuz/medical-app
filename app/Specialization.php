<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    protected $fillable = ['name', 'code', 'status'];

    public $timestamps = true;

    public function doctors()
    {
        return $this->hasMany('App\User');
    }
}
