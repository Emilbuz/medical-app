<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalRecord extends Model
{
    protected $fillable = ['user_id', 'gender', 'blood_type_id', 'dob', 'modified_by', 'created_by', 'birth_place', 'age'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
