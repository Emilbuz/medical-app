<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lab extends Model
{
    protected $fillable = ['user_id', 'date', 'lab_name', 'lab_scan', 'lab_observation'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
